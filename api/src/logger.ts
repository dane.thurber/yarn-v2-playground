import { NextFunction, Request, Response } from 'express'
import * as core from 'express-serve-static-core'
import Logger, { LoggerOptions } from 'bunyan'

import { getConfig } from './config'

export { Logger, LoggerOptions }

export interface CustomRequest<
  P extends core.Params = core.ParamsDictionary,
  ReqBody = any
> extends Request {
  body: ReqBody
  id: string
  logger: Logger
}

export interface LoggerModule extends Logger {
  errorMiddleware: (
    err: Error,
    req: CustomRequest,
    res: Response,
    next: NextFunction
  ) => void
  requestMiddleware: (
    req: CustomRequest,
    res: Response,
    next: NextFunction
  ) => void
}

export function createLogger(
  config: LoggerOptions = createLoggerConfig()
): LoggerModule {
  const logger = new Logger(config) as LoggerModule

  logger.requestMiddleware = (req, res, next) => {
    req.logger = logger.child({ id: req.id })
    req.logger.info({ msg: 'request', req })

    res.on('finish', function() {
      req.logger.info({ msg: 'response', res })
    })

    next()
  }

  logger.errorMiddleware = (err, req, res, next) => {
    req.logger.error({ msg: 'request error', err, req, res })
    next(err)
  }

  return logger
}

export function createLoggerConfig(
  overrides: Partial<LoggerOptions> = {}
): LoggerOptions {
  const config = getConfig()

  const defaults = {
    level: config.logLevel,
    name: 'api',
  }

  const options = Object.assign({}, defaults, overrides)

  return options
}
