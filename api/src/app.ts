import * as bodyParser from 'body-parser'
import express, { Application, Request, Response } from 'express'
import requestId from 'express-request-id'
import helmet from 'helmet'
import responseTime from 'response-time'

import { createContext, createContextMiddleware } from './context'
import { createLogger } from './logger'

export async function getApp() {
  const app: Application = express()

  const logger = createLogger()
  const context = createContext()

  app.use(bodyParser.json())
  app.use(createContextMiddleware(context))

  app.use(helmet())
  app.use(responseTime())
  app.use(requestId())

  app.use(logger.requestMiddleware)

  app.get('/', (_req: Request, res: Response) => {
    res.send('hello world')
  })
  app.use(logger.errorMiddleware)

  return { app, logger }
}
