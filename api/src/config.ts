import * as dotenv from 'dotenv'

type Cache = {
  logLevel: string
  port: number
}

let cache: Cache

const defaults: Partial<Cache> = { logLevel: 'info', port: 3000 }

  // NOTE: create and load config as soon as this file is accessed
;(function loadFromEnv(seed?: Partial<Cache>): Cache {
  dotenv.config()

  const parsed = {
    logLevel: process.env.LOG_LEVEL || defaults.logLevel,
    port: parseInt(process.env.PORT, 10) || defaults.port
  }

  const config = { ...parsed, ...seed }

  return (cache = config)
})()

export function getConfig(): Cache {
  return cache
}
