import { createNamespace, getNamespace } from 'cls-hooked'
import { NextFunction, Request, Response } from 'express'
import { v4 as uuid } from 'uuid'

export interface Context {
  name: string
  get: <T = unknown>(key: string) => T
  set: <T = unknown>(key: string, value: T) => void
}

let cache: Context

export function createContextMiddleware(context: Context = cache) {
  const { name } = context

  return (req: Request, res: Response, next: NextFunction) => {
    const ns = getNamespace(name) || createNamespace(name)

    // NOTE: bind events to they aren't potentially lost
    // https://github.com/jeff-lewis/cls-hooked#namespacebindemitteremitter
    ns.bindEmitter(req)
    ns.bindEmitter(res)

    ns.run(() => {
      ns.set('req', req)
      ns.set('res', res)

      next()
    })
  }
}

export function createContext(name: string = uuid()): Context {
  function get(key: string) {
    const ns = getNamespace(name)
    return ns && ns.active && ns.get(key)
  }

  function set(key: string, value: unknown) {
    const ns = getNamespace(name)
    return ns && ns.active && ns.set(key, value)
  }

  return (cache = { name, get, set })
}

export function getContext(): Context {
  return cache
}
