import { getApp } from './app'
import { getConfig } from './config'

const config = getConfig()

async function main() {
  const { app, logger } = await getApp()
  const { port } = config

  app.listen(port, () => {
    logger.info({ url: `http://localhost:${config.port}` }, 'Listening')
  })
}

main()
