/* eslint-disable @typescript-eslint/no-var-requires */

jest.mock('dotenv')

describe('config', () => {
  afterEach(() => {
    jest.resetModules()
  })

  it('has values', () => {
    const { getConfig } = require('../config')
    const cache = getConfig()

    expect(cache).toMatchObject({
      logLevel: expect.any(String),
      port: expect.any(Number)
    })
  })

  it('loads env config when file is access', () => {
    require('../config')
    const dotenv = require('dotenv')

    expect(dotenv.config).toHaveBeenCalled()
  })

  describe('#ensureVar', () => {
    it.todo('throws if value is undefined')
  })

  describe('#getConfig', () => {
    it.todo('returns the cached config')
  })
})
