/* eslint-env node */
/* eslint-disable @typescript-eslint/no-var-requires */
const baseConfig = require('./jest.config.base')

module.exports = {
  ...baseConfig,
  projects: ['<rootDir>/api', '<rootDir>/client'],

  coverageDirectory: '<rootDir>/coverage/',
  collectCoverageFrom: ['<rootDir>/packages/*/src/**/*.{ts,tsx}']
}
